import React from 'react';
import { View, Text, StyleSheet, ScrollView, Image } from 'react-native';

const ForecastWeather = ({ forecast }) => {
  if (!forecast) return null;

  return (
    <View style={styles.container}>
      <Text style={styles.title}>5-Jours Suivant</Text>
      <ScrollView>
        {forecast.list.map((weather, index) => (
          <View key={index} style={styles.forecastItem}>
            <Text style={styles.date}>{weather.dt_txt}</Text>
            <Text>Temp: {weather.main.temp} °C</Text>
            <Text>{weather.weather[0].description}</Text>
            <Image
              style={styles.icon}
              source={{ uri: `http://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png` }}
            />
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
    borderRadius: 10,
    backgroundColor: '#c2f9ff',
    margin: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 10,
    textAlign: 'center',
  },
  forecastItem: {
    backgroundColor: '#e0f7fa',  
    padding: 15,
    borderRadius: 10,
    marginVertical: 10,
    borderWidth: 1,
    borderColor: '#0288d1',
  },
  date: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  icon: {
    width: 50,
    height: 50,
    marginTop: 5,
  },
});

export default ForecastWeather;
