import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

const CurrentWeather = ({ weather }) => {
  if (!weather) return null;

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Météo du Jour</Text>
      <Text>Ville : {weather.name}</Text>
      <Text>Température: {weather.main.temp} °C</Text>
      <Text>Temps : {weather.weather[0].description}</Text>
      <Image
        style={styles.icon}
        source={{ uri: `http://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png` }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
    borderRadius: 10,
    backgroundColor: '#f9c2ff',
    margin: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 10,
    textAlign: 'center',
  },
  icon: {
    width: 50,
    height: 50,
  },
});

export default CurrentWeather;
